# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Split up mitochondria and actin data by training, validation, testing sets (60: 20: 20)<br />
organellles.py -- separates all data into Mitochondria and Actin folders<br />
exposure.py  -- separates all data into High_Mito, High_Actin, Low_Mito, Low_Actin<br />
training_testing.py -- creates fixed sets for training and testing <br />

