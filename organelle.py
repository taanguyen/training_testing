import os 
import shutil


images = []
os.chdir("C:/Users/TA/Desktop/April 5 2018 actin and mitochondria")

dirpath = os.getcwd()
path_mito = dirpath + "/Mitochondria"
path_act = dirpath + "/Actin"

if not os.path.exists(path_mito):
	os.mkdir(path_mito)
if not os.path.exists(path_act):
    os.mkdir(path_act)


folders = list(filter(lambda x: os.path.isdir(x), os.listdir('.')))
if("Mitochondria" in folders):
	folders.remove("Mitochondria")
if("Actin" in folders):
	folders.remove("Actin")

# specify number of folders to extract data 
n = input("How many data sets do you want to separate?")
if(n == "all"):
	n = len(folders) 

for i in range(int(n)):
	# os.listdir returns a list containing names of entries
	# split actin and mitochondria into separate folders
	for file in os.listdir(dirpath + "/" + folders[i]):
		if (file.endswith("0.tif") or file.endswith("1.tif")):
			if not os.path.isfile(path_mito + "/" + file):
				shutil.copy(dirpath + "/" + folders[i] + "/" + file, path_mito) 
		elif (file.endswith("2.tif") or file.endswith("3.tif")):
			if not os.path.isfile(path_act + "/" + file):
				shutil.copy(dirpath + "/" + folders[i] + "/" + file, path_act) 

