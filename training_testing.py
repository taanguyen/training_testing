import os 
import shutil
import random
#change working directory to folder with all images 
os.chdir("C:/Users/TA/Desktop/April 5 2018 actin and mitochondria")
# create paths for training, testing, and validation sets
dirpath = os.getcwd()
path_train_mito = dirpath + "/Train_Mito"
path_train_act = dirpath + "/Train_Actin"
path_test_mito = dirpath + "/Test_Mito"
path_test_act = dirpath + "/Test_Actin"
path_valid_mito = dirpath + "/Valid_Mito"
path_valid_act = dirpath + "/Valid_Actin"

paths = [path_test_mito, path_test_act, path_train_mito, path_train_act, path_valid_act, path_valid_mito]

# make directories for each data set
#LIMITATION: CAN'T MAKE DATA SET SMALLER - folders won't delete properly :( 
for path in paths:
	#if os.path.exists(path):
	#	shutil.rmtree(path)
	if not os.path.exists(path):
		os.mkdir(path)

folders = list(filter(lambda x: os.path.isdir(x), os.listdir('.')))
#random.shuffle(folders) # use for random shuffling, cross validation

# specify number of folders to extract data 
n = input("How many data sets do you want to separate? ")
if(n == "all"):
	n = len(folders) 
else: n = int(n)
# divide up folder names (strings) into testing, validation, and testing sets
'''
training = folders[: int(n * .6)]
validation = folders[int(n * .6), int(n * .8)]
testing = folders[int(n * .8):]
'''

# high exposure is 0000 and 0001 
# low exposure is 0002 and 0003 

#training data 
for i in range(int(n * .6)):
	# split actin and mitochondria into separate folders
	for file in os.listdir(dirpath + "/" + folders[i]):
		# mitochondria
		if (file.endswith("0.tif") or file.endswith("1.tif")): 
			# make sure file is not duplicated 
			if not os.path.isfile(path_train_mito + "/" + file):
				shutil.copy(dirpath + "/" + folders[i] + "/" + file, path_train_mito) 
		# actin
		elif (file.endswith("2.tif") or file.endswith("3.tif")):
			if not os.path.isfile(path_train_act + "/" + file):
				shutil.copy(dirpath + "/" + folders[i] + "/" + file, path_train_act) 
#validation data 
for i in range(int(n * .6), int(n * .8)):
	for file in os.listdir(dirpath + "/" + folders[i]):
		if (file.endswith("0.tif") or file.endswith("1.tif")):
			if not os.path.isfile(path_valid_mito + "/" + file):
				shutil.copy(dirpath + "/" + folders[i] + "/" + file, path_valid_mito) 
		elif (file.endswith("2.tif") or file.endswith("3.tif")):
			if not os.path.isfile(path_valid_act + "/" + file):
				shutil.copy(dirpath + "/" + folders[i] + "/" + file, path_valid_act) 
#testing data 
for i in range(int(n * .8), n):
	for file in os.listdir(dirpath + "/" + folders[i]):
		if (file.endswith("0.tif") or file.endswith("1.tif")):
			if not os.path.isfile(path_test_mito + "/" + file):
				shutil.copy(dirpath + "/" + folders[i] + "/" + file, path_test_mito) 
		elif (file.endswith("2.tif") or file.endswith("3.tif")):
			if not os.path.isfile(path_test_act + "/" + file):
				shutil.copy(dirpath + "/" + folders[i] + "/" + file, path_test_act) 