import os 
import shutil


images = []
os.chdir("C:/Users/TA/Desktop/April 5 2018 actin and mitochondria")

dirpath = os.getcwd()
path_hm = dirpath + "/High_Mito"
path_lm = dirpath + "/Low_Mito"
path_ha = dirpath + "/High_Actin"
path_la = dirpath + "/Low_Actin"

paths = [path_hm, path_lm, path_ha, path_la]

folders = list(filter(lambda x: os.path.isdir(x), os.listdir('.')))

for path in paths:
	if not os.path.isdir(path):
		os.mkdir(path)

# specify number of folders to extract data 
n = input("How many data sets do you want to separate?")
if(n == "all"):
	n = len(folders) 


for i in range(int(n)):
	filepath = dirpath + "/" + folders[i] + "/" 
	# os.listdir returns a list containing names of entries
	# split actin and mitochondria into separate folders
	for file in os.listdir(dirpath + "/" + folders[i]):
		if (file.endswith("0.tif")):
			if not os.path.isfile(path_hm + "/" + file):
				shutil.copy( filepath + file, path_hm) 
		elif (file.endswith("1.tif")):
			if not os.path.isfile(path_lm + "/" + file):
				shutil.copy(filepath + file, path_lm) 
		elif (file.endswith("2.tif")):
			if not os.path.isfile(path_ha + "/" + file):
				shutil.copy(filepath + file, path_ha) 
		elif (file.endswith("3.tif")):
			if not os.path.isfile(path_la + "/" + file):
				shutil.copy(filepath + file, path_la) 


